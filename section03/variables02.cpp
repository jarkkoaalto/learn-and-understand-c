//============================================================================
// Name        : varaiables02.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : demonstrate different variables
//============================================================================

#include <iostream>
#include <iomanip>
int main() {

	int year, mon = 12, day = 5;
	year = 2018;


	int a = 10, b, c = 14;
	b = a + c;


	std::cout<<"Datetime is: " << std::setw (5)<< year <<std::setw(3)<<mon<<std::setw(3)<<day<<std::endl;
	std::cout<<"sum a + c :"<<b<<std::endl;
	return 0;
}

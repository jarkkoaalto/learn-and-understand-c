//============================================================================
// Name        : varaiables01.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : demonstrate different variables
//============================================================================

#include <iostream>

int main() {

	bool test = true;
	bool test1 = 1;
	bool test2 = false;
	bool test3 = 0;

	unsigned short ushort = 65535;
	signed short sshort = -32267;

	int number = 4;
	long lnumber = 3000400;
	long long llnumber = 30400500606;

	float fnumber = 0.000045;
	double d = 4.55;


	std::cout << "Boolean values " << test <<" "<< test1 <<std::endl;
	std::cout << "Boolean values " << test2 <<" "<< test3 <<std::endl;
	std::cout << "unsignes short " << ushort <<std::endl;
	std::cout << "signed short "<<sshort<<std::endl;
	std::cout << "int number "<<number<<std::endl;
	std::cout << "long number "<<lnumber<<std::endl;
	std::cout << "long long number "<<llnumber<<std::endl;
	std::cout << "float "<<fnumber<<std::endl;
	std::cout << "double "<<d<<std::endl;
	return 0;
}

//============================================================================
// Name        : printlines.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Print different lenght lines an ordered length
//============================================================================

#include <iostream>
#include <iomanip>

int main() {
	std::cout << std::setw (5) << 2018 << " is today's year." <<std::endl;
	std::cout << std::setw (5) << 12 << " is today's month." << std::endl;
	std::cout << std::setw (5) << 5 << " is today's day." << std::endl;
	return 0;
}

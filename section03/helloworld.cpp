//============================================================================
// Name        : helloworld.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Hello,world, only print hello world in the screen
//============================================================================

#include <iostream>

int main() {
	std::cout << "Hello C++ World" << std::endl;
	return 0;
}

/*
 * FriendFunction.cpp
 *
 *  Created on: 27.1.2019
 *      Author: Jarkko
 */

#include <iostream>
#include <string>
#include <stdexcept>

class Date
{
	int day;
	int month;
	int year;
	std::string GetMonthName () const
	{
		static std::string months[] =
		{
				"January","February","March",
				"April","May","June","July",
				"August","Septenber","October",
				"November","December"
		};
		return months[month- 1];

	}

public:

	Date(int d, int m, int y)
	{
		this-> SetDate(d,m,y);
	}
	void SetDate(int d, int m, int y)
	{
		SetDay(d);
		SetMonth(m);
		SetYear(y);
	}
	void Print() const
	{
		std::cout<<this->month <<"/"<<this->day<<"/"<<this->year<<std::endl;
	}

	void PrintNice() const
	{
		std::cout<<day<<" "<<GetMonthName()<<" "<<year<<std::endl;
	}
	int GetDay() const
	{
		return this->day;
	}
	int GetMonth() const
	{
		return this->month;
	}
	int GetYear() const
	{
		return year;
	}
	void SetDay(int d)
	{
		if(d < 0 || d > 31)
			throw std::logic_error("Day out of range");
		this->day = d;
	}
	void SetMonth(int m)
	{
		if(m < 1 || m > 12)
			throw std::logic_error("Mounth must be in [1,12] interval");
		this->month = m;
	}
	void SetYear(int y)
	{
		if(y < 0)
			throw std::logic_error("Year must be positive");
		this->year = y;
	}
};

	class DummyClass
	{
		int dummy_attr1;
		char dummy_attr2;


	public:
		DummyClass() : dummy_attr1(0), dummy_attr2('E')
		{
			std::cout<<"Hello, Me there"<<std::endl;
		}


		DummyClass(int param) : dummy_attr1(param), dummy_attr2('P')
		{
			std::cout<<"2nd counstrutor here. Param = "<<param<<std::endl;
		}

		void Print() const
		{
			std::cout <<"dummy_attr1 = " << dummy_attr1 << std::endl;
			std::cout <<"dummy_att2 = " << dummy_attr2 << std::endl;
		}


		friend int Add(DummyClass dc1, DummyClass dc2)
		{
			int result;
			result = dc1.dummy_attr1 + dc2.dummy_attr1;
			return result;
		}

		~DummyClass()
		{
			std::cout<<"Destructor has been called"<<std::endl;
		}
	};

int main()
{

DummyClass dc1(111), dc2(222);
int res = Add(dc1, dc2);
std::cout<<res<<std::endl;
return 0;
}






/*
 * RuleOfZeroThreeFive.cpp
 *
 *  Created on: 25.1.2019
 *      Author: Jarkko
 */

#include <iostream>

class DummyClass
{
	int dummy_attr1;
	char dummy_attr2;


public:
	DummyClass() : dummy_attr1(0), dummy_attr2('E')
	{
		std::cout<<"Hello, Me there"<<std::endl;
	}


	DummyClass(int param) : dummy_attr1(param), dummy_attr2('P')
	{
		std::cout<<"2nd counstrutor here. Param = "<<param<<std::endl;
	}

	void Print() const
	{
		std::cout <<"dummy_attr1 = " << dummy_attr1 << std::endl;
		std::cout <<"dummy_att2 = " << dummy_attr2 << std::endl;
	}
};


int main()
{
	DummyClass dummy1;
	dummy1.Print();
	DummyClass dummy2(12);
	dummy2.Print();
	return 0;
}

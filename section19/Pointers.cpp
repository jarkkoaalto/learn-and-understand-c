/*
 * Pointers.cpp
 *
 *  Created on: 29.1.2019
 *      Author: Jarkko
*/

#include <iostream>
#include <fstream>
#include <string>



int main()
{
	double a[7] = {1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7};
	double *p;
	double *q;

	p = &a[0];
	q = &a[6];


//	while(q>=p){
//		std::cout<<*q--<<" ";
//	}


	while(p<=q)
		std::cout<<*p++<<" ";





	return 0;
}


/*
 * Pointers.cpp
 *
 *  Created on: 29.1.2019
 *      Author: Jarkko
*/

#include <iostream>
#include <fstream>
#include <string>



int main()
{
	double a[7] = {1,2,3,4,5,6,7};
	double *p = a;

	for(int i=0;i<7;i++)
	{
		std::cout <<i<< " ";
	}

	return 0;
}


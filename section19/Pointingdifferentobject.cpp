/*
 * Pointingdifferentobject.cpp
 *
 *  Created on: 29.1.2019
 *      Author: Jarkko
*/

#include <iostream>
#include <fstream>
#include <string>



int main()
{
	double x = 3.1415;
	double y = 2.719;
	double *p = &x;
	std::cout<< "Thsi is p for x = "<<*p<<std::endl;
	p = &y;
	std::cout<<"This is p for y = "<<*p<<std::endl;

	return 0;
}


/*
 * Pointingdifferentobject.cpp
 *
 *  Created on: 29.1.2019
 *      Author: Jarkko
*/

#include <iostream>
#include <fstream>
#include <string>



int main()
{
	int *p;
	int x = 7;
	p = &x;

	*p = 100;

	std::cout<<"This is p = "<<p<<std::endl;
	std::cout<<"Thsi is 'P = "<<*p<<std::endl;
	std::cout<<"This is x = "<<x<<std::endl;

	return 0;
}


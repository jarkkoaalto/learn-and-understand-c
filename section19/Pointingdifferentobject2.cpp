/*
 * Pointingdifferentobject.cpp
 *
 *  Created on: 29.1.2019
 *      Author: Jarkko
*/

#include <iostream>
#include <fstream>
#include <string>



int main()
{
	int x = 7;
	int y = 6;
	int *p1 = &x;
	int *p2 = &y;

	p1=p2;

	std::cout<<*p1<<std::endl;

	std::cout<<*p2<<std::endl;

	return 0;
}


/*
 * Pointers3.cpp
 *
 *  Created on: 29.1.2019
 *      Author: Jarkko
*/

#include <iostream>
#include <fstream>
#include <string>



int main()
{
	double a[7] = {1.1,2.2,3.3,4.4,5.5,6.6,7.7};
	double *p;
	p = &a[7];

	for(int i=0;i<7;i++)
	{
		p--;
		std::cout <<*p<< " ";

	}

	std::cout<<std::endl <<*p<<std::endl;

	return 0;
}


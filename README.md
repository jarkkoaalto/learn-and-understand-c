# Learn-and-Understand-C++

If you want to learn C++ to advance your skills, gain the ability to program games, and create your own software, you might love this course!  You can go from beginner to advanced with C++ using this course because it has 9+ hours of video that might motivate you to keep learning and programming in C++!

Cource Content:

###### Section  1: Introduction
###### Section  2: Set Up
###### Section  3: First C++ Program
###### Section  4: Interaction with user
###### Section  5: More advanced examples
###### Section  6: Repeating code executions - loops
###### Section  7: Arrays - basics
###### Section  8: Functions - basics
###### Section  9: Exception handling
###### Section 10: Structures in C++
###### Section 11: Generic programming - basics
###### Section 12: STL (standard types in C++)
###### Section 13: Classes in C++ - Beginning
###### Section 14: Classes in C++ - Basics
###### Section 15: Decomposition
###### Section 16: Interaction with WinAPI
###### Section 17: Files in C++ - Basics
###### Section 18: Basic Encryption
###### Section 19: Pointers and References
//============================================================================
// Name        : staticAttributes.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Static Attributes in the C++
//============================================================================
/*
#include <iostream>
#include <string>

struct Date
	{
		int day;
		int month;
		int year;
	};

struct Book
{
	int ID;
	std::string name;
	std::string author;
	Date published;
	static int Count;
};

int Book::Count;

int main()
{
	Book b1, b2;
	b1.ID = 0;
	b1.name = "Inferno";
	b1.author = "Dan Brown";
	b1.Count = 1;

	b2.ID = 1;
	b2.name = "Jason Bourn";
	b2.author = "Robert Ludlum";

	std::cout<<b1.Count<<":"<<b1.ID<<": "<< b1.name<<", "<< b1.author<<std::endl;
	b2.Count=10;
	std::cout<<b2.Count<<std::endl;


	return 0;
}

*/

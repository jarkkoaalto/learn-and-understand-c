/*
 * LambdaFunction.cpp
 *
 *  Created on: 13.1.2019
 *      Author: Jarkko


#include<iostream>

int main()
{
	auto lambda01 = [](int x)
	{
		return x * x * x;
	};

	auto lambda02 = [] (int x, int y) -> int
	{
		return x * y;
	};

	int num = 10;
	int result = lambda01(num);
	int result02 = lambda02(10,5);



	return 0;
}

*/

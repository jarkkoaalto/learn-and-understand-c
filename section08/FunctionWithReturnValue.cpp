/*
 * FunctionWithReturnValue.cpp
 *
 *  Created on: 11.1.2019
 *      Author: Jarkko
 *
 *      ask user a number and return last digit -> 111, last_digit = 1


#include <iostream>

void Print(int n);
int GetLastDigit(const int n);


int main(){

	int number;
	std::cout<<"Enter number :";
	std::cin >> number;
	int last_digit = GetLastDigit(number);

	std::cout<<"Last digit of: " << number << " is : "<< last_digit << std::endl;

	return 0;
}

void Print(int n)
{
	for(int i=0; i<=n;i++)
	{
		std::cout<<"*";
	}
	std::cout<<std::endl;
}

int GetLastDigit(const int n)
{
	//int result = n % 10;
	//return result;
	// OR
	return n%10;
}
 */

//============================================================================
// Name        : DoWhileloop.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : DoWhile loop example

//============================================================================

#include <iostream>
#include <cmath>

int main() {
	int n = 0;
	do
	{
		std::cout<<"Enter natural number n: ";
		std::cin >>n;
	}
	while(n <=0);

	double sqrt_n = std::sqrt(n);
	std::cout <<"Sqrt (" <<n<<") = "<< sqrt_n <<std::endl;



	return 0;
}


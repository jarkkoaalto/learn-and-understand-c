//============================================================================
// Name        : NestedLoop.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : NestedLoop example

//============================================================================

#include <iostream>

int main() {

	int N,M;
	std::cout<<"Enter M and N, separated by space: ";
	std::cin>>M>>N;

	for(int i = 1; i<= M; ++i)
	{
		for(int j = 1; j <= i; ++j){
			std::cout<<"*";
		}
		std::cout << std::endl;
	}
	return 0;
}


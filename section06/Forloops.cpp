//============================================================================
// Name        : Forloops.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : For loops
//============================================================================

#include <iostream>

int main() {
	int n;
	std::cout<<"Enter number (int) :";
	std::cin>>n;

	int sum = 0;
	for(int i=1; i <= n; ++i){
		int number;
		std::cout<<"Enter: "<<i<<" . number :"<<std::endl;
		std::cin >>number;

		sum = sum + number; // sum is
	}
	std::cout<<"Sum of "<<n <<"entered numbers is : "<<sum <<std::endl;
	return 0;
}


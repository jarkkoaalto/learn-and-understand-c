//============================================================================
// Name        : BreakAndContinue.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Break And Continue example

//============================================================================

#include <iostream>

int main() {

	int sum = 0;

	// Break example
	for(int i = 1; i<=50;++i)
	{
		int number;
		std::cout<<"Enter number (or -1 to exit) :";
		std::cin>>number;

		if(number == -1)
		{
			break;
		}
		sum += number;
	}

	std::cout<<"Sum is: " << sum<<std::endl;

	// Continue example
	for(int i=0;i<=20;++i)
	{
		if(i%3 ==0)  // All 3 divided number is removed
			continue;

		std::cout<<i<<" ";
	}
	std::cout<<" "<<std::endl;

	for(int i=20; i>=1;--i){
		if(i%3 != 0)
			continue;

		std::cout<<i<<" ";
	}

	return 0;
}


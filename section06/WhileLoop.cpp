//============================================================================
// Name        : Whileloop.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : While loop example

//============================================================================

#include <iostream>

int main() {
	int n = 0;
	std::cout<<"Enter number :";
	std::cin>>n;
	int sum = 0;
	int i = 1;

	while(i < n){
		int number;
		std::cout<<"Enter "<< i << " . number: ";
		std::cin >> number;
		sum += number;
		++i;
	}

std::cout<<"Sum of "<<n<<" Entered number is "<<sum;

	return 0;
}


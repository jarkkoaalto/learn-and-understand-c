/*
 * Map.cpp
 *
 *  Created on: 23.1.2019
 *      Author: Jarkko
 */

#include <iostream>
#include <string>
#include <map>
#include <typeinfo>


int main()
{
	std::map<int, std::string> numbers;

	numbers[0] = "Zero";
	numbers[1] = "Two";
	numbers[2] = "Two";

	std::cout <<numbers.at(0) <<std::endl;
	std::cout <<numbers.size() <<std::endl;
	bool emp = numbers.empty();

	auto it = numbers.find(222);
	auto it1 = 10;
	if(it == numbers.end())
		std::cout << "Key not found! " << numbers.at(3)<< std::endl<<std::endl;
	else
		std::cout << "Key found! Value:" <<std::endl<<std::endl;

	std::map<int, std::string> Days
	{
		{1, "Monday"},
		{2, "Tuesday"},
		{3, "Wednesday"},
		{4, "Thurdsay"},
		{5, "Firday"},
		{6, "Saturday"},
		{7, "Sunday"}
	};

	for(int i = 1; i <= 7; ++i)
		std::cout << Days[i] << std::endl;

	return 0;
}

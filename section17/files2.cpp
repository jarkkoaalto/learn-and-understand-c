/*
 * files.cpp
 *
 *  Created on: 28.1.2019
 *      Author: Jarkko
 *
 */

#include <iostream>
#include <string>
#include <fstream>

int main()
{

	std::ofstream file("random.txt");

	if(!file)
		std::cout<<"File does not exist or you do not have a permission to write to it";
	else
			file << "Hi there";


	file.close();

	return 0;
}


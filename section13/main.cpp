//============================================================================
// Name        : main.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Principles of OOP in the C++
//============================================================================

#include <iostream>

class Date
{

int day;
int month;
int year;

public:

	void SetDate(int d, int m, int y)
	{
		this->day=d;
		this->month=m;
		this->year=y;
	}

	void Print() const
	{
		std::cout<<this -> day << "." <<this->month <<"."<<this->year<<std::endl;
	}


	int GetDay()const{return this -> day;}
	int GetMonth()const{return this -> month;}
	int GetYear()const{return this -> year;}

	void SetDay(int d){this->day = d;}
	void SetMonth(int m){this->month = m;}
	void SetYear(int y){this->year = y;}

};


int main() {


	int d,m,y;
	std::cout<<"Enter date: (day.month,year)";
	std::cin >> d >> m >> y;
	Date dt;
	dt.Print();
	std::cout<<"Month :"<<dt.GetMonth();

	return 0;
}

/*
 * ConstantVariables.cpp
 *
 *  Created on: 7.12.2018
 *      Author: Jarkko
 *  This program claculate sum
 */

#include <iostream>
#define PI 3.1415

int main()
{
	int x = 5;
	const int y = 3;
	int const w = 100;

	std::cout<<"Enter circle radius: "<<std::endl;
	float radius;
	std::cin>>radius;
	float circle_area = PI * radius * radius;
	std::cout<<"Circle's radius : " << circle_area <<std::endl;

	float mystery = x * y % w;
	std::cout<<"Mystery number is : "<<mystery <<std::endl;

	return 0;
}

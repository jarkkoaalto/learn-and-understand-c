/*
 * autokeywordcpp
 *
 *  Created on: 7.12.2018
 *      Author: Jarkko
 *  This program intoducted c++11 auto keyword
 */

#include <iostream>

int main()
{
	auto x = 10.5;
	auto y = 10;

	int var1 = 10;
	int var2(10); // constructor initialization
	int var3{33};

	auto var{55};

	return 0;
}

//============================================================================
// Name        : standardinput.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style. Interaction with the user
//============================================================================

#include <iostream>


int main() {

	int number;
	std::cout<<"Enter an integer: "<<std::endl;
	std::cin>> number;

	int squared = number * number;
	std::cout<<"This number squared is "<< squared <<std::endl;
	std::cout<<"This number " << number <<" squared is " << number * number << std::endl;
	return 0;
}

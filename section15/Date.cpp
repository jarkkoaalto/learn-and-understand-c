/*
 * Date.cpp
 *
 *  Created on: 28.1.2019
 *      Author: Jarkko
 */

#include "Date.h"


void Date::SetDate(int d, int m, int y)
{
	this -> day = d;
	this -> month = m;
	this -> year = y;
}

void Date::Print() const
{
	std::cout<<this->day << " : "<< this->month <<" : "<<this->year;
}

int Date::GetDay() const
{
	return this->day;
}

int Date::GetMonth() const
{
	return this->month;
}

int Date::GetYear() const
{
	return this->year;
}

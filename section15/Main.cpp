/*
 * GenericClasses.cpp
 *
 *  Created on: 27.1.2019
 *      Author: Jarkko
*/
#include <string>
#include <stdexcept>
#include "Date.h"

template<typename TYPE>
class DummyGeneric
{
	TYPE Attribute;
	public :
		DummyGeneric(TYPE Param) : Attribute(Param)
		{

		}
		TYPE GetAttribute() const
		{
			return this -> Attribute;
		}

		void SetAttribute(TYPE a)
		{
			this->Attribute = a;
		}

};

int main()
{
	Date dt;
	dt.SetDate(10, 10, 2010);
	dt.Print();
return 0;
}


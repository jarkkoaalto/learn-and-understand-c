/*
 * Date.h
 *
 *  Created on: 28.1.2019
 *      Author: Jarkko
 */

#ifndef SECTION15_DATE_H
#define DATE_H

#include<iostream>

class Date
{
	int day;
	int month;
	int year;

public:
	void SetDate(int d, int m, int y);
	void Print() const;
	int GetDay() const;
	int GetMonth() const;
	int GetYear() const;
};

#endif //SECTION15_DATE_H

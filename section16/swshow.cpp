/*
 * swshow.cpp
 *
 *  Created on: 28.1.2019
 *      Author: Jarkko
 *
 * Using OS Libraries and functions
 * AdvApi32.dll
 *
 *
 *


#include <iostream>
#include <windows.h>


int main()
{

	std::cout<<"We've got 'SW_SHOW' constant : " << SW_SHOW <<std::endl;
	// We've got 'SW_SHOW' constant : 5
	// https://docs.microsoft.com/en-us/windows/desktop/api/winuser/nf-winuser-showwindow
	// SW_SHOW 5 -> Activates the window and displays it in its current size and position.
	std::cout<<GetLastError();
	return 0;
}
 */

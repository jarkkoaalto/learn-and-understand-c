//============================================================================
// Name        : ArrayInitilization01.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Array Initilization in C++, Ansi-style
//============================================================================

#include <iostream>
#include <array>

int main() {
	int arr1[5] = {12,55,66,1,4};
	// std::cout<<arr1[2];

	for(int i=0;i<5;i++){
		std::cout<<arr1[i]<<std::endl;
	}
	std::cout<<std::endl;

	int arr2[100] = {};
	for(int i=0;i<100;i++){
		arr2[i] = 5;
		std::cout<<arr2[i]<<std::endl;
	}



	return 0;
}

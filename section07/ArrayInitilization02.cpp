//============================================================================
// Name        : ArrayInitilization02.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Array Initilization02 in C++, Ansi-style
//============================================================================

#include <iostream>
#include <array>

int main() {
	int arr1[100] = {0};
	for(int i=0; i<100;++i)
	{
		arr1[i] = i;
		std::cout<<arr1[i]<<std::endl;
	}

	std::cout<<std::endl;

	#define TEST 10
	int arr2[TEST];
	for(int i=0;i<10;i++){
		std::cout<<arr2[i]<<std::endl;
	}



	return 0;
}

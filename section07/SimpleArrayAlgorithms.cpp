//============================================================================
// Name        : SimpleArrayAlgorithms.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Simple array Algorithms in C++, Ansi-style
//============================================================================

#include <iostream>
#include <array>

int main() {

	const int noe = 10;
	int arr1[10];

	for(int i = 0;i<noe;++i){
	std::cout<<"Enter "<< i+1 << " . number: ";
	std::cin>>arr1[i];
	}

	int MAX = arr1[0];

	for(int i=1;i<noe;++i)
	{
		if(arr1[i]>MAX)
		{
			MAX = arr1[i];
		}
	}

	std::cout<<"MAX is : "<<MAX<<std::endl;

	int MIN = arr1[0];
	for(int i=1;i<noe;++i)
		{
			if(arr1[i]<MIN)
			{
				MIN = arr1[i];
			}
		}

		std::cout<<"MIM is: "<<MIN<<std::endl;
	return 0;
}

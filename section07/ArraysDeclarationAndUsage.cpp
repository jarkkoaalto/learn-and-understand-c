//============================================================================
// Name        : ArraysDeclarationAndUsage.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Arrays Declaration And Usage in C++, Ansi-style
//============================================================================

#include <iostream>
#include <array>

int main() {
	int i = 0;
	int arr1[10];
	float arr2[3];

	arr1[0] = 5;
	arr1[2] = 10;
	arr1[4] = 34;
	arr1[1] = 1;
	arr1[9] = 22;

	arr2[2] = 0.22;
	arr2[0] = 100.003;
	arr2[1] = 11;

	for(i = 0; i<10;i++){
		std::cout<<arr1[i]<<std::endl;
	}
	std::cout<<std::endl;
	for(i = 0; i<3;i++){
		std::cout<<arr2[i]<<std::endl;
	}


	return 0;
}

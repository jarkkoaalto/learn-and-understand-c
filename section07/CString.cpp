//============================================================================
// Name        : CString.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : C String in C++, Ansi-style
//============================================================================

#include <iostream>
#include <array>

int main() {

	char str[6] = {'H','e','i','p','s','\0'};
	std::cout<<str<<std::endl;

	return 0;
}

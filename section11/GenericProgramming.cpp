/*
 * GenericProgramming.cpp
 *
 *  Created on: 20.1.2019
 *      Author: Jarkko
 */

#include <iostream>
using namespace std;

template <typename TYPE>

void Print(TYPE arr[], int len)
{
	cout <<"[";
	for(int i=0;i<len -1; ++i)
		cout<<arr[i] <<", ";
	if(len != 0)
	{
		cout<<arr[len-1];
		cout<<"]"<<endl;
	}
}

int main()
{
	int arr00[] = {2,3,4,5,6,1000,222,44,-100};

	double arr01[] = {2.0,3.3,4.3,5.6,6.0,1000.0,22.02,44.0,-100.0};
	//const int len = sizeof arr01 / sizeof arr01[0];
	char arr03[] =  {'t','i','i','n','a'};
	// print out
	// 36 4
	// [2, 3, 4, 5, 6, 1000, 222, 44, -100]
	// 4 because in arr[0] store int 2 and int reseive 4 bytes
	// 4 bytes / all elements in array
	// and divide

	cout<<sizeof arr01 <<" "<<sizeof arr01[0]<<endl;
	Print(arr01,9);
	Print(arr00,9);
	Print(arr03,5);
	return 0;
}



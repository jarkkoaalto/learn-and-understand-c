//============================================================================
// Name        : CommaOperators.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Commma operators
//============================================================================

#include <iostream>

int main() {
	int x = 2;
	int y = 3;

	// comma way
	if(x > 4, y > 1)
		std::cout<<"Something... ok, true"<<std::endl;

	// right way
	if(x > 4 && y > 1)
			std::cout<<"Something... ok, true"<<std::endl;

	return 0;
}


//============================================================================
// Name        : ifelsestatements.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : conditional statement if else
//============================================================================

#include <iostream>

int main() {

	int age;
	std::cout<<"Enter person's age: ";
	std::cin>>age;

	if(age >= 18){
		std::cout<<"Person is an adult!"<<std::endl;
	}
	else if(age > 13){
		std::cout<<"Person is a teenager!"<<std::endl;
	}
	else if(age > 3){
		std::cout <<"Person is a kid"<<std::endl;
	}
	else{
		std::cout <<"Baby"<<std::endl;
	}
	return 0;
}


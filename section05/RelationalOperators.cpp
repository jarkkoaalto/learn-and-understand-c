//============================================================================
// Name        : RelationalOperators.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Relational operators
//============================================================================

#include <iostream>

int main() {
	int x = 10, y = 5, z = 15, w = 10;

	bool rel1 = x>y; // true
	bool rel2 = y>x; // false
	bool rel3 = x==z; // false
	bool rel4 = x != z; // true

	std::cout<<"rel1 = "<<rel1<<"\nrel2 = "<<rel2<<"\nrel3 = "<<rel3<<"\nrel4 = "<<rel4<<std::endl;

	bool rel5 = x >= w; // true
	bool rel6 = !(x>w); // true
	std::cout<<"\nrel5 = "<<rel5<<"\nrel6 = "<<rel6<<std::endl;

	bool rel7 = (12>x) > 7; // false
	bool rel8 = 12 > x;
	bool rel9 = 12 > x && x > 7;
	std::cout<<"\nrel7 = "<<rel7<<"\nrel8 = "<<rel8<<"\nrel9 = "<<rel9<<std::endl;


	return 0;
}


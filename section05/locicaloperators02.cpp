//============================================================================
// Name        : locicaloperators.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Logical operators 02
//============================================================================

#include <iostream>

int main() {

	bool A = true;
	bool B = true;
	bool C = false;
	bool D = true;

	bool n1 = !A;
	std::cout<<"!A = "<<n1<<std::endl;

	bool n2 = !C;
	std::cout<<"!C = "<<n2<<std::endl;

	bool n3 = A && !C && D;
	std::cout<<"A && !C && D = "<<n3<<std::endl;

	bool n4 = !A || !B || !C || !D;
	std::cout<<"!A || !B || !C || !D = "<<n4<<std::endl;

	return 0;
}


//============================================================================
// Name        : locicaloperators.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : Logical operators
//============================================================================

#include <iostream>

int main() {

	bool A = true;
	bool B = true;
	bool C = false;
	bool D = true;

	bool and1 = A && B;
	bool and2 = A && B && C && D;
	std::cout<<"A=true\nB=true\nC=false\nD=true\n"<<std::endl;

	std::cout<<"A && B = " <<and1<<std::endl;
	std::cout<<"A && B && C && D = "<<and2<<std::endl;

	bool or1 = C || D;
	bool or2 = A || D || B;
	bool and_or1 = (A && B) || C;
	std::cout<<"C || D= "<<or1<<std::endl;
	std::cout<<"A || D || B = "<<or2<<std::endl;
	std::cout<<"A && B || C = "<<and_or1<<std::endl;

	return 0;
}


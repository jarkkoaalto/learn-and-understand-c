//============================================================================
// Name        : ifelsestatements.cpp
// Author      : Jarkko Aalto
// Version     :
// Copyright   : Your copyright notice
// Description : conditional statement if else
//============================================================================

#include <iostream>

int main() {

	int x = 10;
	if(x>10){
		std::cout<<"X is greater than 10" <<std::endl;
	}
	else{
		std::cout<<"X is not greater than 10"<<std::endl;
	}

	if(x > 2){
		std::cout<<"OK, x > 2"<<std::endl;
	}

	return 0;
}

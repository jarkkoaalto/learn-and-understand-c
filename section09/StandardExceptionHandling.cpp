/*
 * ExceptionHandling.cpp
 *
 *  Created on: 17.1.2019
 *      Author: Jarkko



#include <iostream>
#include <cmath>
#include <stdexcept>

void something(int n){
	if(n == 0)
		throw 1;
	if(n == -1)
		throw 'c';
}

int main(){

	// different error what can handling
	// std::logic_error
	// std::domain_error;
	// std::invalid_argument
	// std::length_error;
	// std::overflow_error
	// std::underflow_error;

	int num;
	std::cout<<"Enter number: ";
	std::cin>>num;

	try{
		something(num);

	}catch(...)
	{
		std::cout << "ERROR:" << num <<std::endl;
	}
	std::cout << num;

	return 0;
}

*/

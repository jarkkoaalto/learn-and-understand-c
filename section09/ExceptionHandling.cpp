/*
 * ExceptionHandling.cpp
 *
 *  Created on: 17.1.2019
 *      Author: Jarkko



#include <iostream>
#include <cmath>
#include <stdexcept>

using namespace std;

double Sqrt(double x){
	if(x < 0)
		throw "Negative number !";

	double sroot = std::pow(x,0.5);
	return sroot;
}
void something(int n)
{
	if(n == 0)
		throw 1;
	if(n < 0 && n > -100)
		throw "YASS over 0 but least -100";
	if(n == -1)
		throw 'c';
	if(n > -101)
		throw domain_error("Some std exception");
}

int main(){

	int num;

	std::cout <<"Enter number:";
	std::cin>>num;


	try {
		something(num);
	}catch(char c)
	{
		cout<<"Caugtht : CHAR"<< endl;
		return -1;
	}catch(int i){
		cout<<"Caught: INT"<<endl;
	}catch(const char *txt){
		cout<<"Caught Text: "<< txt;
	}catch(domain_error de){
		cout<<"Caught domain_error with text: " << de.what();
	}
	return 0;
}
 */
